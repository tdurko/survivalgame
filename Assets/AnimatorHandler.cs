﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHandler : MonoBehaviour
{
    #region Singleton
    public static AnimatorHandler instance;

    public void Awake()
    {

        if (instance != null)
        {
            Debug.LogWarning("More than One instance of AnimatorHandler");
            return;
        }
        instance = this;
    }
    #endregion

    public Blade blade;

    public void EnableTrigger()
    {
        blade.gameObject.GetComponent<BoxCollider>().enabled = true;
    }



    public void DisableTrigger()
    {
        blade.gameObject.GetComponent<BoxCollider>().enabled = false;
    }
}
