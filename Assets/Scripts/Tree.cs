﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : Destructible
{
    public TreeDown treeDown;


    public float axeResistance = 0f;

    public override void TakeHit(Weapon weapon)
    {
        TakeDamage(weapon.timberDamage - axeResistance);
    }
    public override void Destruct()
    {
        treeDown.AddRigidAndForce();
    }
}
