﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public float HP = 100f;
    public virtual void TakeHit(Weapon weapon)
    {
        
    }

    public virtual void Destruct()
    {
        Destroy(this.gameObject);
    }

    public void TakeDamage(float damage)
    {
        HP = -damage;
        if(HP <= 0)
        {
            Destruct();
        }
    }
}
