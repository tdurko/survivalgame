﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public virtual void Interact()
    {
        Debug.Log("Interakcja");
    }

    public void Focus()
    {
        Debug.Log("Press E to interact with " + gameObject.name);
    }

    public void ClearFocus()
    {

    }
}
