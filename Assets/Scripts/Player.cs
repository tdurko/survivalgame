﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Inventory inventory;
    #region Singleton
    public static Player instance;


    public void Awake()
    {

        if (instance != null)
        {
            Debug.LogWarning("More than One instance of Player");
            return;
        }
        instance = this;
    }
    #endregion

    public Animator animator;

    public Weapon weapon;

    Interactable focus;
    
     public void SetFocus(Interactable interactable)
    {
        focus = interactable;
        interactable.Focus();
    }
    public void ClearFocus()
    {
        if(focus != null)
        {
            focus.ClearFocus();
            focus = null;
        }
        
    }

    public void Interact()
    {
        if (focus != null)
        {
            focus.Interact();
        }
            
    }


}
