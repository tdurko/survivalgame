﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Weapon", menuName = "Inventory/Wepon")]
public class Weapon : Item
{
    public float enemyDamage = 10f;
    public float timberDamage = 10f;
    public float stoneDamage = 10f;
    public override void UseItem()
    {
        Player.instance.weapon = this;
        Debug.Log("Attack Attack witg " + name);
        Player.instance.animator.SetTrigger("attack");
    }
}