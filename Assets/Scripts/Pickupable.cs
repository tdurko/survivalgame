﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickupable : Interactable
{
    public Item item;

    public override void Interact()
    {
        base.Interact();
        Debug.Log(name + " has been pick up");
        Player.instance.inventory.AddItem(item);
        Destroy(gameObject);
    }
}
