﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    List<Item> items = new List<Item>();
    int size = 30;
    Item currentEquiped;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool AddItem(Item item)
    {
        if(items.Count < size)
        {
            Debug.Log(item.name + " has been added to inventory");
            items.Add(item);
            currentEquiped = item;
            return true;
        }
        else
        {
            
            Debug.Log("Inventory full");
            return false;
        }
        
    }
    public bool UseCurrentEquiped()
    {
        if (currentEquiped != null)
        {
            currentEquiped.UseItem();

            return true;
        }
        else
        {
            return false;
        }

    }

    public bool TakeToHand(Transform hand)
    {
        if (currentEquiped != null)
        {
            GameObject.Instantiate(currentEquiped.prefab, hand);
            return true;
        }
        else
        {
            return false;
        }

    }

    public bool CheckIfCosumable()
    {
        if (currentEquiped.GetType() == typeof(Food))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    public void DeleteCurrentEquip()
    {
        items.Remove(currentEquiped);
        currentEquiped = null;
    }
}
