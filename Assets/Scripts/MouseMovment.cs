﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovment : MonoBehaviour
{
    public float mouseSens = 100f;

    public Camera cam;

    public Transform playerTransform;

    public float maxDistance = 5;

    float xRotation = 0f;
    void Start()
    {
      // Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Interactable interactable;
        if (Physics.Raycast(ray, out hit, maxDistance))
        {
            interactable = hit.collider.GetComponentInParent<Interactable>();
            if (interactable != null)
            {
                Player.instance.SetFocus(interactable);
            }
            else
            {
                Player.instance.ClearFocus();
            }
        }
        else
        {
            Player.instance.ClearFocus();
        }


        float x = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float y = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        xRotation -= y;

        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        playerTransform.Rotate(Vector3.up * x);
    }


}
