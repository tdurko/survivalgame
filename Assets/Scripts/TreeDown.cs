﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeDown : MonoBehaviour
{
    public static int size;
    public GameObject[] Logs = new GameObject[size];
    public GameObject treeBranch;
    public Transform top;

    public Vector3 forceVector;
    Rigidbody rigid;
    public bool falling = false;
    float time = 0f;
    public void AddRigidAndForce()
    {
        rigid = this.gameObject.GetComponent<Rigidbody>();
        
        
        rigid.isKinematic = false;
        rigid.AddForceAtPosition(forceVector, top.position);
        falling = true;
        time = Time.time + 1f;
    }
    private void Update()
    {
        if(falling && rigid.velocity.magnitude == 0 && Time.time >= time)
        {
            foreach(GameObject log in Logs)
            {
                log.SetActive(true);
            }
            treeBranch.SetActive(false);
            Destroy(rigid);
            Destroy(this.GetComponent<BoxCollider>());
            falling = false;
        }
    }




}
