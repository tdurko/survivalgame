﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Destructible
{
    public float armour = 0f;
    public override void TakeHit(Weapon weapon)
    {
       TakeDamage(weapon.enemyDamage - armour);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
