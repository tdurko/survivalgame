﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{
    Weapon weapon;
    BoxCollider box;
    float time = 0f;


    void Start()
    {
        box = this.gameObject.GetComponent<BoxCollider>();
        AnimatorHandler.instance.blade = this;

    }

    public void SetWeapon(Weapon weapon)
    {
        this.weapon = weapon;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision.name + " get touched by Blade");
        SetWeapon(Player.instance.weapon);
        Destructible hit = collision.GetComponentInParent<Destructible>();
        if (hit != null)
        {
            if(Time.time >= time)
            {
                hit.TakeHit(weapon);
            }
            
        }
    }

}
