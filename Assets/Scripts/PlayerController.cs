﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform handPoint;
    bool itemInHand = false;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            Player.instance.Interact();
        }

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attack();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            Equip();
        }
    }

    void Attack()
    {
        // Attact with current weapon;
        if(itemInHand)
        {
            if (Player.instance.inventory.UseCurrentEquiped())
            {
                if (Player.instance.inventory.CheckIfCosumable())
                {
                    Debug.Log("Jedzonko zjedzono");
                    Player.instance.inventory.DeleteCurrentEquip();
                    Destroy(handPoint.GetChild(0).gameObject);
                    itemInHand = false;
                }
            }
        }

        
    }

    public void Equip()
    {
        if(!itemInHand)
        {
            if(Player.instance.inventory.TakeToHand(handPoint))
            {
                itemInHand = true;
            }
            
        }
        else
        {

            Destroy(handPoint.GetChild(0).gameObject);
            itemInHand = false;
        }
        
    }
}
