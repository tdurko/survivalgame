﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    public GameObject prefab;
    public Sprite sprite;
    int amount = 1;
    public int stack = 16;
    
    public virtual void UseItem()
    {
        Debug.Log("Use " + name);
    }

    public bool ConsumeItem()
    {
        if(amount > 1)
        {
            amount--;
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool AddAmount()
    {
        if(amount < stack)
        {
            amount++;
            return true;
        }
        else
        {
            return false;
        }
    }

    public int GetAmount()
    {
        return amount;
    }

   


}
